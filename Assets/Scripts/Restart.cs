﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Restart : MonoBehaviour {

	[SerializeField] GameObject restartPrompt;
	
	public void RestartLab () {
		SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
	}

}
