﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PhMeter : MonoBehaviour {

	[SerializeField] LineRenderer lr;
	[SerializeField] GameObject recorder;
	[SerializeField] Transform recorderTop, meterTop, holsteredPosition;

	///<summary>This shouldn't be modifiable by other code, so this is read-only.</summary>
	public Transform HolsteredPosition {
		get {
			return holsteredPosition;
		}
	}

	[SerializeField] Text phText;

	readonly float verticalOffset = 0.5f;

	void Start () {
		lr = GetComponentInChildren <LineRenderer> ();
		lr.positionCount = 4;

		recorder.GetComponent <PhRecorder> ().parentMeter = gameObject;
	}
	
	void FixedUpdate () {
		if (recorderTop != null && meterTop != null) {
			Vector3 aboveMeter = meterTop.position;
			aboveMeter.y += 0.25f;

			Vector3 aboveRecorder = recorderTop.position;
			aboveRecorder.y += 0.25f;

			if (aboveMeter.y < aboveRecorder.y) {
				aboveMeter.y = aboveRecorder.y;
			} else {
				aboveRecorder.y = aboveMeter.y;
			}

			Vector3[] points = new Vector3[] {meterTop.position, aboveMeter, aboveRecorder, recorderTop.position};
			lr.SetPositions (points);
		}
	}

	public void SetText (double ph) {
		if (ph >= 0f && ph <= 14f) {
			phText.text = ph.ToString ("00.00");
		} else {
			phText.text = "NaN";
		}
	}

	void OnDestroy () {
		if (recorder != null) {
			Destroy (recorder);
		}
	}

	public void ChildDestroyed () {
		Destroy (gameObject);
	}
	
}
