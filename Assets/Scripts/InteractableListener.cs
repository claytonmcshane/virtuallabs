﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableListener : MonoBehaviour {

    BoxCollider2D box;

	void Start () {
        box = GetComponent<BoxCollider2D> ();
        MouseSelection.OnColliderStateChange += OnInteractableStateChange;
	}

    void OnDisable () {
        MouseSelection.OnColliderStateChange -= OnInteractableStateChange;
    }

    void OnInteractableStateChange () {
        box.enabled = !box.enabled;
    }

}
