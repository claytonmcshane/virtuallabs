﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnedObject : MonoBehaviour {

	[SerializeField] ObjSpawner parentSpawner;

	///<summary>This is called automatically when the object is about to be destroyed. DO NOT MANUALLY CALL THIS.
	///This will only destroy the Ph Meter parent of a Ph Recorder.</summary>
	void OnDestroy () {
		if (parentSpawner != null) {
			parentSpawner.CopyDeleted ();
		}
	}

	///<summary>Overwrites the parent spawner so this game object can inform its 'parent' when it is being destroyed.</summary>
	public void OverwriteParent (ObjSpawner objSpawner) {
		this.parentSpawner = objSpawner;
	}
	
}
