﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackTransform : MonoBehaviour {

	public Transform transformToFollow;
	public bool worldSpace = true;
	public Camera cam;

	void Start () {
		if (!worldSpace && cam == null) {
			Debug.LogWarning ("No camera selected in track transform for gameobject " + gameObject.name);
			worldSpace = true;
		}
	}

	void LateUpdate () {
		if (worldSpace) {
			transform.SetPositionAndRotation (transformToFollow.position, new Quaternion ());
		} else {
			transform.SetPositionAndRotation (cam.WorldToScreenPoint (transformToFollow.position), new Quaternion ());
		}
	}
}
