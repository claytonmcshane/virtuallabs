﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformManager : MonoBehaviour {

	public GameObject[] objectsToHideInWebPlayer;
    public GameObject howTo;

	void Start () {
#if (UNITY_STANDALONE || UNITY_WEBGL)
        howTo.SetActive (true);
#endif

#if UNITY_EDITOR
        Debug.Log ("Editor");
        howTo.SetActive (false);
#endif

        if (Application.platform == RuntimePlatform.WebGLPlayer) {
			foreach (GameObject go in objectsToHideInWebPlayer) {
				go.SetActive (false);
			}
		}
	}

}
