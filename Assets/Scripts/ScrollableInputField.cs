﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ScrollableInputField : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler, IPointerClickHandler {

	//script from https://forum.unity.com/threads/inputfield-s-inside-a-scrollview.446976/#post-3823456
	ScrollRect _scrollRect = null;
	InputField _input = null;
	bool _isDragging = false;

	void Start () {
		_scrollRect = GetComponentInParent <ScrollRect> ();
		_input = GetComponent <InputField> ();
		_input.DeactivateInputField ();
		_input.enabled = false;
	}

	public void OnBeginDrag (PointerEventData data) {
		if (_scrollRect != null && _input != null) {
			_isDragging = true;
			_input.DeactivateInputField();
			_input.enabled = false;
			_scrollRect.SendMessage("OnBeginDrag", data);
		}
	}

	public void OnEndDrag (PointerEventData data)
	{
		if (_scrollRect != null && _input != null) {
			_isDragging = false;
			_scrollRect.SendMessage ("OnEndDrag", data);
		}
	}

	public void OnDrag (PointerEventData data) {
		if (_scrollRect != null && _input != null) {
			_scrollRect.SendMessage ("OnDrag", data);
		}
	}

	public void OnPointerClick (PointerEventData data)	{
		if (_scrollRect != null && _input != null) {
			if (!_isDragging && !data.dragging) {
				_input.enabled = true;
				_input.ActivateInputField ();
			}
		}
	}
}
