﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpreadsheetContainer : MonoBehaviour {

	public Text indexLabel;
	public InputField xValue, yValue;
	
	public void DeleteRow () {
		Destroy (gameObject);
	}

}
