﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (BoxCollider2D))]
public class MouseHover : MonoBehaviour {

	MouseSelection ms;

	void Start () {
		ms = GameObject.FindGameObjectWithTag ("MainCamera").GetComponent <MouseSelection> ();
	}

	void OnMouseEnter () {
		ms.HoveringOverObject (gameObject.tag);
	}

	void OnMouseExit () {
		ms.NotHovering ();
	}

}
