﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhRecorder : MonoBehaviour {

	public GameObject parentMeter;

	void OnDestroy () {
		if (parentMeter != null) {
			parentMeter.GetComponent <PhMeter> ().ChildDestroyed ();
		}
	}

	public void ResetParentText () {
		if (transform.parent == null) {
			parentMeter.GetComponent <PhMeter> ().SetText (00.00);
		}
	}
}
