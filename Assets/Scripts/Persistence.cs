﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;

public class Persistence : MonoBehaviour {

	private readonly string folderName = "/Saves/";
	private readonly string configFile = "/Settings.cfg";
	private readonly string csvExt = ".csv";
	private readonly string txtExt = ".txt";

	[SerializeField] Text notepadText;

	[SerializeField] GameObject promptObj;
	[SerializeField] Text promptText;

	void Start () {
		//CheckDataDirectory ();
		//ReadConfig ();
	}

	/* void CheckDataDirectory () {
		if (!Directory.Exists (saveFileAddress + folderName)) {
			Directory.CreateDirectory (saveFileAddress + folderName);
			Debug.Log ("Created directory");
		} else {
			Debug.Log ("Directory exists");
		}

		if (!File.Exists (saveFileAddress + configFile)) {
			File.Create (saveFileAddress + configFile);
			Debug.Log ("Created config file");
		} else {
			Debug.Log ("Config file exists");
		}
	}

	public void UpdateConfig () {
		StreamWriter sw = new StreamWriter (saveFileAddress + configFile);

		sw.Close ();
	}

	public void ReadConfig () {
		StreamReader sr = new StreamReader (saveFileAddress + configFile);
		//sr.Read ();
		sr.Close ();
	}*/

	public void Save () {
		if (Application.platform == RuntimePlatform.WebGLPlayer) {
			Debug.Log (notepadText.text + "\n" + SpreadsheetToText ());
			GUIUtility.systemCopyBuffer = notepadText.text + "\n" + SpreadsheetToText ();
			ShowPrompt ("Copied notepad and spreadsheet data to clipboard. Paste into a text document, cut the spreadsheet data and save that to another file as a .csv, which can be opened using Excel");
			//ShowPrompt ("Cannot save notepad or spreadsheet data while this program is running in Web GL Player.");
		} else {
			try {
				//Debug.Log (notepadText.text + "\n" + SpreadsheetToText ());
				//GUIUtility.systemCopyBuffer = (notepadText.text + "\n" + SpreadsheetToText ());
				string notesLoc = ExportNotes ();
				string spreadsheetLoc = ExportSpreadsheet ();
				string message = string.Format ("Exported notepad and spreadsheet to Documents/Virtual Lab/{0} and {1}", notesLoc, spreadsheetLoc);
				ShowPrompt (message);
			} catch (Exception e) {
				Debug.LogError (e.Message);
				ShowPrompt ("Encountered an error while trying to save.");
			}
		}
		
	}

	///<summary>Tries to export user notes to the Documents folder.</summary>
	public string ExportNotes () {
		DateTime localDate = DateTime.Now;
		DateTime utc = DateTime.UtcNow;
		string fileName = localDate.Year.ToString () + "-" + localDate.Month + "-" + localDate.Day + " " + localDate.TimeOfDay.Hours + "-" + localDate.TimeOfDay.Minutes + "-" + localDate.TimeOfDay.Seconds;
		Debug.Log (fileName);

		//if an error occurs here when trying to save, it would prevent the program from attempting to save the spreadsheet
		//this try-catch should allow execution to continue.
		try {
			if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsEditor) {
				string folder = Directory.GetParent (Environment.GetFolderPath (Environment.SpecialFolder.ApplicationData)).FullName;
				if (Environment.OSVersion.Version.Major >= 6) {
					folder = Directory.GetParent (folder).ToString ();
				}
				string fileDest = folder + "\\Documents\\Virtual Labs\\" + fileName + txtExt;
				Debug.Log (fileDest);

				if (!Directory.Exists (folder + "\\Documents\\Virtual Labs")) {
					Debug.Log ("directory didnt exist");
					Directory.CreateDirectory (folder + "\\Documents\\Virtual Labs");
				}

				if (!File.Exists (fileDest)) {
					Debug.Log ("file didnt exist");
					FileStream fs = File.Create (fileDest);
					fs.Close ();
				}

				StreamWriter sw = new StreamWriter (fileDest);
				string text = notepadText.text;
				sw.Write (text);
				sw.Close ();
			} else if (Application.platform == RuntimePlatform.LinuxPlayer || Application.platform == RuntimePlatform.LinuxEditor
				|| Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.OSXEditor) {
				string folder = Path.Combine (Environment.GetFolderPath (Environment.SpecialFolder.Personal), "Documents");
				string fileDest = folder + "\\Virtual Labs\\" + fileName + txtExt;

				if (!Directory.Exists (folder + "\\Documents\\Virtual Labs")) {
					Directory.CreateDirectory (folder + "\\Documents\\Virtual Labs");
				}

				if (!File.Exists (fileDest)) {
					FileStream fs = File.Create (fileDest);
					fs.Close ();
				}

				StreamWriter sw = new StreamWriter (fileDest);
				string text = notepadText.text;
				sw.Write (text);
				sw.Close ();
			}
		} catch (Exception e) {
			Debug.LogError (e.Message);
		}

		return fileName;
		//ShowExportPrompt (true, fileDest);
	}

	public string SpreadsheetToText () {
		List <SpreadsheetContainer> rows = new List<SpreadsheetContainer> ();
		rows.AddRange (Resources.FindObjectsOfTypeAll <SpreadsheetContainer> ());
		rows.Reverse ();
		rows.RemoveAt (0);
		string spreadsheet = "";
		foreach (SpreadsheetContainer row in rows) {
			string line = string.Format ("{0}, {1}\n", row.xValue.text, row.yValue.text);
			spreadsheet += line;
		}
		return spreadsheet;
	}

	///<summary>Tries to export the spreadsheet to the Documents folder.</summary>
	public string ExportSpreadsheet () {
		//building the filename, saving it as a .csv so we can open it in Excel
		DateTime localDate = DateTime.Now;
		DateTime utc = DateTime.UtcNow;
		string fileName = localDate.Year.ToString () + "-" + localDate.Month + "-" + localDate.Day + " " + localDate.TimeOfDay.Hours + "-" + localDate.TimeOfDay.Minutes + "-" + localDate.TimeOfDay.Seconds;
		Debug.Log (fileName);

		List <SpreadsheetContainer> rows = new List<SpreadsheetContainer> ();
		//this includes the prefab of the row we have, so we need to remove that one from showing up.
		rows.AddRange (Resources.FindObjectsOfTypeAll <SpreadsheetContainer> ());
		rows.Reverse ();
		rows.RemoveAt (0);

		try {
			if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsEditor) {
				string folder = Directory.GetParent (Environment.GetFolderPath (Environment.SpecialFolder.ApplicationData)).FullName;
				if (Environment.OSVersion.Version.Major >= 6) {
					folder = Directory.GetParent (folder).ToString ();
				}
				string fileDest = folder + "\\Documents\\Virtual Labs\\" + fileName + csvExt;
				
				Debug.Log (fileDest);

				if (!Directory.Exists (folder + "\\Documents\\Virtual Labs")) {
					Debug.Log ("directory didnt exist");
					Directory.CreateDirectory (folder + "\\Documents\\Virtual Labs");
				}

				if (!File.Exists (fileDest)) {
					Debug.Log ("file didnt exist");
					FileStream fs = File.Create (fileDest);
					fs.Close ();
				}

				StreamWriter sw = new StreamWriter (fileDest);
				//write each row to the file
				foreach (SpreadsheetContainer vals in rows) {
					//Debug.Log ("X " + vals.xValue.text);
					//Debug.Log ("Y " + vals.yValue.text);
					string line = string.Format ("{0}, {1}", vals.xValue.text, vals.yValue.text);
					sw.WriteLine (line);
				}

				sw.Close ();
			} else if (Application.platform == RuntimePlatform.LinuxPlayer || Application.platform == RuntimePlatform.LinuxEditor
				|| Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.OSXEditor) {
				string folder = Path.Combine (Environment.GetFolderPath (Environment.SpecialFolder.Personal), "Documents");
				string fileDest = folder + "\\Virtual Labs\\" + fileName + txtExt;

				if (!Directory.Exists (folder + "\\Documents\\Virtual Labs")) {
					Directory.CreateDirectory (folder + "\\Documents\\Virtual Labs\\");
				}

				if (!File.Exists (fileDest)) {
					FileStream fs = File.Create (fileDest);
					fs.Close ();
				}

				StreamWriter sw = new StreamWriter (fileDest);
				//write each row to the file
				foreach (SpreadsheetContainer vals in rows) {
					//Debug.Log ("X " + vals.xValue.text);
					//Debug.Log ("Y " + vals.yValue.text);
					string line = string.Format ("{0}, {1}", vals.xValue.text, vals.yValue.text);
					sw.WriteLine (line);
				}

				sw.Close ();
			}
		} catch (Exception e) {
			Debug.LogError (e.Message);
		}
		

		return fileName;
	}

	public void Load () {

	}

	///<summary>Displays a text prompt informing the user of the location of the newly saved file. You do not need to check the platform before calling this, as this function does it anyway.</summary>
	void ShowExportPrompt (bool notePad, string location) {
		switch (Application.platform) {
			default:
				if (notePad) {
					promptText.text = string.Format ("Exported notes to {0}", location);
				} else {
					promptText.text = string.Format ("Exported spreadsheet to {0}", location);
				}
				break;
			case RuntimePlatform.WebGLPlayer:
				promptText.text = "Sorry, notes and spreadsheets cannot be exported from the web browser.";
				break;
		}

		promptObj.SetActive (true);
	}

	void ShowPrompt (string text) {
		promptText.text = text;
		promptObj.SetActive (true);
	}
}
