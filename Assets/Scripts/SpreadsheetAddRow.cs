﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpreadsheetAddRow : MonoBehaviour {

	public Transform contentObject;
	public GameObject rowPrefab;

	public void AddRow () {
		GameObject row = Instantiate (rowPrefab, contentObject) as GameObject;
	}

}
