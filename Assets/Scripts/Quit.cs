﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quit : MonoBehaviour {

	[SerializeField] GameObject quitPrompt;

	public void ConfirmExit () {
		Application.Quit ();
	}

}
