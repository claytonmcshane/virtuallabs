﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StorageToggle : MonoBehaviour {

	[SerializeField] float startPosX = 0f;
	[SerializeField] float endPosX = 0f;

	///<summary>Having the speed below 1 results in the UI freezing almost exactly halfway when moving back towards its starting position.</summary>
	[SerializeField] [Range (1.1f, 4f)] float speed = 2f;
	[SerializeField] RectTransform objectToMove;

	public void Toggle () {
        if (Extensions.ValueInRange (objectToMove.anchoredPosition.x, startPosX, 0.1f)) {
			StartCoroutine (MoveObjectToEndPosition ());
			//objectToMove.anchoredPosition = new Vector2 (endPosX, 0);
		} else {
			StartCoroutine (MoveObjectToStartPosition ());
			//objectToMove.anchoredPosition = new Vector2 (startPosX, 0);
		}
	}

    IEnumerator MoveObjectToEndPosition () {
		while (objectToMove.anchoredPosition.x != endPosX) {
			float newX = Mathf.Lerp (objectToMove.anchoredPosition.x, endPosX, speed);
			objectToMove.anchoredPosition = new Vector2 (newX, 0);
			yield return null;
		}
	}

	IEnumerator MoveObjectToStartPosition () {
		while (objectToMove.anchoredPosition.x != startPosX) {
			float newX = Mathf.Lerp (objectToMove.anchoredPosition.x, startPosX, speed);
			objectToMove.anchoredPosition = new Vector2 (newX, 0);
			yield return null;
		}
	}

}
