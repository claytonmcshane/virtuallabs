﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public enum MixtureState {
	Empty = 0,		//nothing in the container
	Undiluted = 1,	//single liquid in container
	Unmixed = 2,	//2 liquids in container, not stirred
	Mixed	= 3		//2 liquids in container, stirred
}

public static class Extensions {
	public static bool ValueInRange (float currentValue, float middleValue, float variation) {
		return (currentValue >= (middleValue - variation) && currentValue <= (middleValue + variation));
	}
}

///<summary>A mess of a file.</summary>
public class Contents : MonoBehaviour {

	///<summary>The name of this container as set by the user.</summary>
	public string setName = "";

	///<summary>The initial contents of this container. Cannot be empty.</summary>
	public string contents;

	///<summary>If it is undiluted or mixed it can have its PH read.</summary>
	public MixtureState mixtureState;

	///<summary>The initial volume of the first contents added to this container, in milliliters.</summary>
	public float volume = 0f;
	
	///<summary>The volume of water currently in this container. Do not modify in editor if changing volume of water container.</summary>
	public float waterVolume = 0f;

	///<summary>The KA of the acid added initially to this container. Leave if it is a basic.</summary>
	public float ka = 0f;
	///<summary>The PKA of the acid added initially to this container. Leave if it is a basic.</summary>
	public float pKa = 0f;

	///<summary>The minimum volume of liquid that must be added to this container. Used for pipettes.</summary>
	public float minVolume = 0f;
	///<summary>The maximum volume this container can hold.</summary>
	public float maxVolume = 0f;
	///<summary>The current volume in milliliters, rounded to 3 decimal places.</summary>
	public float CurrentVolume {
		get {
			if (contents == "Distilled Water" && secondarySolutionName == "") {
				if (volume > waterVolume) {
					return volume;
				} else {
					return waterVolume;
				}
			}
			if (!usePKA) {
				float f = volume + waterVolume + secondaryVolume;
				float.TryParse(f.ToString ("0.000"), out f);
				return f;
			} else {
				float f = waterVolume + basicVol + acidicVol;
				float.TryParse (f.ToString ("0.000"), out f);
				return f;
			}
		}
	}

	///<summary>Molar value</summary>
	public float molarValue = 0f;
	///<summary>The molar value of the solution, rounded to 3 decimal places.</summary>
	public float MolarValue {
		get {
			float f;
			float.TryParse (molarValue.ToString ("0.000"), out f);
			return f;
		}
	}
	///<summary>PH of the solution.</summary>
	public double _PH;
	public double PH {
		get {
			if (mixtureState == MixtureState.Mixed || mixtureState == MixtureState.Undiluted) {
				if (!usePKA) {
					return _PH;
				} else {
					return phFromPka;
				}
			} else {
				return -1;
			}
		}
	}

	[Header ("Secondary")]
	///<summary>The molar value of the secondary solution.</summary>
	public float secondarySolutionMolar = 0;
	///<summary>The initial volume of the secondary solution.</summary>
	public float secondaryVolume = 0;
	///<summary>The name of the secondary solution in this mixture. Do not leave blank if you have secondary contents in this container initially, otherwise secondary contents may be erased when adding more to this container.</summary>
	public string secondarySolutionName = "";
	///<summary>Initial KA of the secondary solution in this mixture. Only modify if your secondary is an acid.</summary>
	public float secondaryKA = 0;
	public float secondaryPKA = 0;

	[Header ("pKa formula")]
	public float firstAcidPka = 0;
	public float acidicMols = 0;
	public float acidicVol = 0;
	public float basicMols = 0;
	public float basicVol = 0;
	public float bufferMolarity {
		get {
			return (float) ((Math.Abs (basicMillimoles) + Math.Abs(acidMillimoles)) / CurrentVolume);
		}
	}
	public float basicMillimoles = 0;
	public float acidMillimoles = 0;

	///<summary>This is the initial PKA value added to the container. This one is used for the PKA formula. Do not modify in editor.</summary>
	[SerializeField] double _phPKA = 0;
	public double phFromPka {
		get {
			return _phPKA;
		}
	}
	
	///<summary>A list of solutions added to this container in the format: (Acid/Basic) (Name) (Molarity). Do not modify directly in editor.</summary>
	public List<string> solutionNamesAndMolarity = new List<string> ();
    public Dictionary<string, float> solutionVolumes = new Dictionary<string, float> ();

    public Material filledTexture;
    [Range (0, 1)] public float fillMatMin = 0;
    [Range (0, 1)] public float fillMatMax = 1;

	///<summary>The public PH variable depends on this, as it is used to determine whether it should use the PKA formula, or the other one to calculate and return the correct PH value.</summary>
	public bool usePKA = false;

	void Start () {
		if (volume == 0) {
			mixtureState = MixtureState.Empty;
		} else {
			mixtureState = MixtureState.Undiluted;
		}

		if (contents != "") {
			setName = contents;
		} else {
			setName = gameObject.tag;
		}

		if (contents != "Distilled Water" && volume > 0) {
			if (_PH < 7) {
				firstAcidPka = pKa;
				acidicMols = molarValue;
				acidicVol = volume;
				solutionNamesAndMolarity.Add ("Acid " + contents + " " + molarValue);
                solutionVolumes.Add (contents + " " + molarValue, volume);
            } else {
				basicMols = molarValue;
				basicVol = volume;
				solutionNamesAndMolarity.Add ("Basic " + contents + " " + molarValue);
                solutionVolumes.Add (contents + " " + molarValue, volume);
            }
		} else if (contents == "Distilled Water") {
            solutionVolumes.Add (contents, waterVolume);
        }

		acidMillimoles = acidicMols * acidicVol;
		basicMillimoles = basicMols * basicVol;

        if (tag == "Beaker" || tag == "Cylinder" || tag == "Pipette" || tag == "Solution") {
            foreach (Renderer renderer in transform.GetComponentsInChildren<Renderer>()) {
                //Debug.Log (renderer.material.name);
                if (renderer.material.name.Contains ("FillMaterial")) {
                    filledTexture = renderer.material;
                    UpdateFillTexture ();
                }
            }
        }
	}

	public void CalculatePhWithPKA () {
		if (firstAcidPka != 0) {
			
			if (basicMols == 0) {
				_phPKA = firstAcidPka + UnityEngine.Random.Range (-0.05f, 0.05f);
				return;
			}
			if (acidicMols == 0) {
				usePKA = false;
				_phPKA = -1;
				return;
			}

			double d = Math.Log10 (basicMillimoles / acidMillimoles);
			//0 numerator or denominator with 0.05M buffer molarity
			if (basicMillimoles <= 0 && Extensions.ValueInRange (bufferMolarity, 0.05f, 0.025f)) {
				//Debug.Log ("Basic millimoles <= 0 and buffer is around 0.05");
				_phPKA = (firstAcidPka + Math.Log10 (0.06f / acidMillimoles) + UnityEngine.Random.Range (-0.05f, 0.05f));
				return;
			} else if (acidMillimoles <= 0 && Extensions.ValueInRange (bufferMolarity, 0.05f, 0.025f)) {
				//Debug.Log ("Acidic millimoles <= 0 and buffer is around 0.05");
				_phPKA = (firstAcidPka + Math.Log10 (0.0004f / acidMillimoles) + UnityEngine.Random.Range (-0.05f, 0.05f));
				return;
			}

			//if they use 125ml basic, 62.5ml acid
			if (acidMillimoles <= 0 && Extensions.ValueInRange (bufferMolarity, 0.5f, 0.05f)) {
				_phPKA = (firstAcidPka + Math.Log10 (125 / 0.0045f)) + UnityEngine.Random.Range (-0.05f, 0.05f);
				return;
			}

			//values are within reasonable spectrum
			if (basicMillimoles > 0 && acidMillimoles > 0) {
				_phPKA = (firstAcidPka + Math.Log10 (basicMillimoles / acidMillimoles) + UnityEngine.Random.Range (-0.05f, 0.05f));
				//Debug.Log ("first acid pka " +firstAcidPka);
				//Debug.Log ("basic " + basicMillimoles);
				//Debug.Log ("acid " + acidMillimoles);
				//Debug.Log (_phPKA);
			} else if (basicMillimoles < 0 && acidMillimoles < 0) {
				_phPKA = (firstAcidPka + Math.Log10 (basicMillimoles / acidMillimoles) + UnityEngine.Random.Range (-0.05f, 0.05f));
			}
		} else {
			_phPKA = -1;
		}
	}

	///<summary>Adds to the volume of the primary contents in this container. </summary>
	public void ChangeVolumeOfContents (float vol) {
        this.volume += vol;
		if (volume <= 0) {
			ResetContents ();
		}
        UpdateFillTexture ();
    }

	public void RemoveContentsByPercentage (float vol) {
        //Debug.Log ("remove contents by percentage");
		float percentage = vol / (volume + secondaryVolume + waterVolume);
        RemovePercentFromVolumesDict (percentage);
		float deltaPrimary = (percentage * this.volume);
		float deltaSecondary = (percentage * this.secondaryVolume);
		float deltaWater = (percentage * this.waterVolume);
        //Debug.Log (deltaPrimary);
        //Debug.Log (deltaSecondary);
        //Debug.Log (deltaWater);
        this.volume -= deltaPrimary;
		this.secondaryVolume -= deltaSecondary;
		this.waterVolume -= deltaWater;
        UpdateFillTexture ();
    }

    public void CombineMixtureVolumes (Dictionary<string, float> compoundAndVolumes, float percentage) {
        foreach (KeyValuePair<string, float> kvp in compoundAndVolumes) {
            if (solutionVolumes.ContainsKey (kvp.Key)) {
                solutionVolumes [kvp.Key] += (kvp.Value * percentage);
            } else {
                solutionVolumes.Add (kvp.Key, (kvp.Value * percentage));
            }
        }
    }

    void RemovePercentFromVolumesDict (float percentage) {
        //Debug.Log ("removing percent from volumes dict " + percentage);
        Dictionary<string, float> temp = new Dictionary<string, float> ();
        foreach (KeyValuePair <string, float> kvp in solutionVolumes) {
            temp [kvp.Key] = kvp.Value - (kvp.Value * percentage);
        }
        solutionVolumes.Clear ();
        solutionVolumes = temp;
    }

	public void AddMixtureToContainer (Contents c, float volume) {
		Debug.Log ("Contents is " + c.contents);
		
		AddMixtureToContainer (c.solutionNamesAndMolarity.ToArray (), c, volume);
		//Debug.Log ("secondary contents is empty");

		float percentage = volume / (c.acidicVol + c.basicVol + c.waterVolume);
		float deltaAcid = (percentage * c.acidicVol);
		float deltaBasic = (percentage * c.basicVol);
		float deltaWater = (percentage * c.waterVolume);
		float deltaAcidMillimoles = (percentage * c.acidMillimoles);
		float deltaBasicMillimoles = (percentage * c.basicMillimoles);

        CombineMixtureVolumes (c.solutionVolumes, percentage);

		//Debug.Log ("List of solutions and molarities in previous contents");
		//Debug.Log ("water "+ deltaWater);
		//Debug.Log ("delta acid " + deltaAcid);
		//Debug.Log ("delta basic " + deltaBasic);
		//Debug.Log ("delta acid millimoles " + deltaAcidMillimoles);
		//Debug.Log ("delta basic millimoles " + deltaBasicMillimoles);

		//DO NOT DIRECTLY MODIFY WATER VOLUME OF THE OTHER CONTENTS BELOW THIS POINT.
		//YOU ALSO DO NOT NEED TO MODIFY PRIMARY OR SECONDARY CONTENTS ATTRIBUTES. THERE IS A SEPARATE FUNCTION FOR IT.

		Debug.Log (solutionNamesAndMolarity.Count);
		if (solutionNamesAndMolarity.Count == 0 && c.contents != "Distilled Water") {
			//Debug.Log ("0 contents");
			c.acidicVol -= deltaAcid;
			c.basicVol -= deltaBasic;
			c.acidMillimoles -= deltaAcidMillimoles;
			c.basicMillimoles -= deltaBasicMillimoles;
			acidicVol += deltaAcid;
			acidicMols += c.acidicMols;

			basicVol += deltaBasic;
			basicMols += c.basicMols;

			acidMillimoles = deltaAcidMillimoles;
			basicMillimoles = deltaBasicMillimoles;
			foreach (string s in c.solutionNamesAndMolarity) {
				if (!solutionNamesAndMolarity.Contains (s)) {
					solutionNamesAndMolarity.Add (s);
				}
			}
			//Debug.Log ("Buffer molarity " + bufferMolarity);
		} else if (c.contents == "Distilled Water") {
			//DO NOT MODIFY THIS. THIS NEEDS TO BE IN HERE BEFORE THE ELSE OTHERWISE
			//IT WILL NOT BE ABLE TO RUN THE CORRECT CODE FOR ADDING NON-WATER ONLY MIXES
			//Debug.Log ("Delta water" + deltaWater);
			//Debug.Log ("Buffer molarity " + bufferMolarity);
		} else {
			c.acidicVol -= deltaAcid;
			c.basicVol -= deltaBasic;
			c.acidMillimoles -= deltaAcidMillimoles;
			c.basicMillimoles -= deltaBasicMillimoles;
			acidicVol += deltaAcid;
			basicVol += deltaBasic;

			//sodium hydroxide strength doesn't get affected by acetic acid, so we take the strength of the basic from the new acid
			//this fix is for when the user adds the basic solution to the buffer container before they add the acid
			if (solutionNamesAndMolarity.Contains ("Basic Sodium Hydroxide 1") || solutionNamesAndMolarity.Contains ("Basic Sodium Hydroxide 10")) {
				//Debug.Log ("we have a basic in the solution");
				if (c.solutionNamesAndMolarity.Contains ("Acid Acetic Acid 1") || c.solutionNamesAndMolarity.Contains ("Acid Acetic Acid 0.1")) {
					//Debug.Log ("and we're adding acetic acid");
					float actualAcid = (deltaAcidMillimoles - basicMillimoles);
					//Debug.Log (actualAcid);
					if (actualAcid < 0) {
						actualAcid = 0;
					}
					//Debug.Log (actualAcid);
					acidMillimoles += actualAcid;
				} else {
					acidMillimoles += deltaAcidMillimoles;
				}
			} else {
				acidMillimoles += deltaAcidMillimoles;
			}

			//concentration of basics will not drop unless hydrochloric acid is added (or other acids not covered in this lab)
			if (basicMillimoles > 0 && basicVol > 0 && c.solutionNamesAndMolarity.Contains ("Acid Hydrochloric Acid 1")) {
				basicMillimoles -= deltaAcidMillimoles;
			}
			if (acidMillimoles > 0 && acidicVol > 0 && deltaBasicMillimoles > 0) {
				acidMillimoles -= deltaBasicMillimoles;
			}

			basicMillimoles += deltaBasicMillimoles;

			foreach (string s in c.solutionNamesAndMolarity) {
				if (!solutionNamesAndMolarity.Contains (s)) {
					string[] line = s.Split (' ');
					string type = line[0];
					string molarValue = line [(line.Length -1)];
					float f = 0;
					float.TryParse (molarValue, out f);
					if (type == "Acid") {
						acidicMols += f;
					} else {
						basicMols += f;
					}
					solutionNamesAndMolarity.Add (s);
				}
			}
		}

		if (this.firstAcidPka == 0) {
			if (c.pKa != 0) {
				firstAcidPka = c.pKa;
			} else {
				firstAcidPka = c.firstAcidPka;
			}
		}

		if (solutionNamesAndMolarity.Count > 2 || c.usePKA || (solutionNamesAndMolarity.Count == 2 && waterVolume > 0)) {
			usePKA = true;
		}

		CalculatePhWithPKA ();
		c.CheckVolumeLevels ();
        UpdateFillTexture ();
        CheckParentObject ();
		//Debug.Log ("Buffer molarity " + bufferMolarity);
	}

	public void AddSecondaryMixtureToContainer (Contents c, float deltaVolume) {
		if (this.secondarySolutionName == "" && c.secondarySolutionName != "") {
			this.secondaryKA = c.secondaryKA;
			this.secondarySolutionMolar = c.secondarySolutionMolar;
			this.secondarySolutionName = c.secondarySolutionName;
			this.secondaryVolume = deltaVolume;
			this.secondaryPKA = c.secondaryPKA;
		}
	}

	public void AddMixtureToContainer (string[] addedSolutionNames, Contents c, float totalVolume) {
		float percentage = totalVolume / c.CurrentVolume;
		float deltaPrimary = c.volume * percentage;
		float deltaSecondary = c.secondaryVolume * percentage;
		float deltaWater = c.waterVolume * percentage;
		//Debug.Log ("List of solutions and molarities in previous contents - add mix to container [] c vol");
		//Debug.Log (deltaPrimary);
		//Debug.Log (deltaSecondary);
		//Debug.Log (deltaWater);

		if (deltaWater > 0) {
			usePKA = true;
			waterVolume += deltaWater;
		}

		if (volume == 0 && c.contents != "Distilled Water") {
			//Debug.Log ("Volume == 0. Adding to primary");
			this._PH = c._PH;

			this.volume = deltaPrimary;
			this.pKa = c.pKa;
			this.ka = c.ka;
			this.molarValue = c.molarValue;

			mixtureState = MixtureState.Undiluted;
			this.contents = c.contents;
			//Debug.Log ("Previously empty container");

			AddSecondaryMixtureToContainer (c, deltaSecondary);
			c.CheckVolumeLevels ();
		} else if (c.contents == "Distilled Water") {
			CalculatePhWithPKA ();
			usePKA = true;
			c.CheckVolumeLevels ();

            if (CurrentVolume == waterVolume) {
                _PH = 7 + UnityEngine.Random.Range (-0.05f, 0.05f);
            }
		} else {
			//Debug.Log ("Volume did not equal 0");
			//check if they're the same compound
			if (contents == c.contents && this.molarValue == c.molarValue) {
				//Debug.Log ("Same contents");
				volume += deltaPrimary;
				mixtureState = MixtureState.Undiluted;
				c.CheckVolumeLevels ();
				CalculatePhWithPKA ();
				return;
			}

			if (secondarySolutionName == c.contents && secondarySolutionMolar == c.molarValue) {
				secondaryVolume += deltaPrimary;
			}

			//this must be after the solution name and molar value checks so we don't double the added volume.
			if (secondarySolutionName == "") {
				secondarySolutionName = c.contents;
				secondaryVolume = deltaPrimary;
				secondarySolutionMolar = c.molarValue;
				secondaryKA = c.ka;
				secondaryPKA = c.pKa;
			}

			AddSecondaryMixtureToContainer (c, deltaSecondary);
            mixtureState = MixtureState.Unmixed;
			c.CheckVolumeLevels ();
			//DO NOT MODIFY VOLUME LEVELS OF OTHER SOLUTION CONTENTS PAST THIS POINT

			//check if they aren't both acidic or basic
			if ((c.ka == 0 && ka > 0) || (c.ka > 0 && ka == 0)) {
				//Debug.Log ("One acid, and one basic");
				//acid is in primary variables, basic in secondary
				if (ka > 0) {
					CalculateNewPH (MolarValue, volume, ka, secondarySolutionMolar, secondaryVolume);
				} else {
					CalculateNewPH (secondarySolutionMolar, secondaryVolume, secondaryKA, MolarValue, volume);
				}

				CalculatePhWithPKA ();

				
                CheckParentObject ();
                return;
			}
            CheckParentObject ();
        }
	}

	///<summary></summary>
	void CalculateNewPH (float acidicMol, float acidicVol, double acidKA, float basMol, float basVol) {
		if ((basMol == 10f && basVol == 1f && acidicMol == 0.1f && acidicVol == 100f) || (Extensions.ValueInRange (basVol, 1f, 0.005f) && Extensions.ValueInRange (acidicVol, 100f, 0.005f))) {
			Debug.Log ("Basic molar is 10, basic vol is 1mL");
			basVol = 0.99994f;
		}
		double acidicMoles = (acidicVol * acidicMol);
		//Debug.Log (acidicMoles);
		double basicMoles = (basVol * basMol);
		//Debug.Log (basicMoles);

		double origLeft = acidicMoles - basicMoles;
		double con = origLeft / (acidicVol + basVol + waterVolume);
		double secCon = basicMoles / (basVol + acidicVol + waterVolume);
		double hPlus = (acidKA * con) / secCon;

		//Debug.Log ("Con " + con);
		//Debug.Log ("Sec con " + secCon);
		//Debug.Log ("H Plus " + hPlus);

		double ph = 0 - Math.Log10 (hPlus);
		Debug.Log ("added vol" + basicVol + "; ph: " + ph);
		if (basicVol == 0.99994f) {
			basicVol = 1f;
		}

		_PH = Mathf.Clamp ((float) ph + UnityEngine.Random.Range (-0.05f, 0.05f), 0.001f, 14f);
		if (double.IsNaN (_PH)) {
			usePKA = true;
		}
	}

	void ResetContents () {
		Debug.Log ("In reset");
		StartCoroutine (ResetContentsInXTime (0.5f));
	}

	IEnumerator ResetContentsInXTime (float time) {
		yield return new WaitForSeconds (time);
		_PH = 0;
		this.molarValue = 0;
		contents = "";
		volume = 0;
		ka = 0;
		pKa = 0;
		secondarySolutionMolar = 0;
		secondarySolutionName = "";
		secondaryKA = 0;
		secondaryVolume = 0;

		firstAcidPka = 0;
		acidicMols = 0;
		acidicVol = 0;
		basicVol = 0;
		basicMols = 0;

		waterVolume = 0;

		mixtureState = MixtureState.Empty;

		usePKA = false;

		basicMillimoles = 0;
		acidMillimoles = 0;
		
		solutionNamesAndMolarity.Clear ();
        solutionVolumes.Clear ();
        UpdateFillTexture ();
		_phPKA = 0;
	}

	public override string ToString () {
		string s = "";
		if (secondarySolutionName == "") {
			s = contents;
		} else {
			s = "";
		}
		return s;
	}

	public void CheckVolumeLevels () {
		Debug.Log ("Checking volume levels of " + gameObject.name + "| got " + CurrentVolume);
		StartCoroutine (CheckVolumeLevels (0.25f));
	}

	IEnumerator CheckVolumeLevels (float time) {
		yield return new WaitForSeconds (time);
		if (CurrentVolume <= 0f) {
			StartCoroutine (ResetContentsInXTime (0.5f));
		}
	}

    void UpdateFillTexture () {
        if (filledTexture != null) {
            //Debug.Log (gameObject.name);
            //Debug.Log (CurrentVolume / maxVolume);
            float f = (fillMatMax - fillMatMin) * (CurrentVolume / maxVolume);
            filledTexture.SetFloat ("_CutoffY", fillMatMin + f);
        }
    }

    void CheckParentObject () {
        if (transform.parent != null) {
            Debug.Log ("Has parent");
            if (transform.parent.tag == "Stirrer") {
                Debug.Log ("Parent is stirrer");
                mixtureState = MixtureState.Mixed;
            }
        }

        PhRecorder phRecorder = GetComponentInChildren<PhRecorder> ();
        if (phRecorder != null) {
            phRecorder.parentMeter.GetComponent<PhMeter> ().SetText (PH);
        }
    }

}
