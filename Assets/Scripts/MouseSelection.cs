﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MouseSelection : MonoBehaviour {

	[SerializeField] bool transferring = false;
	
	[Header ("Selected Object Link")]
	[SerializeField] GameObject selectedObject;
	[SerializeField] string heldObjectPreviousTag;
	Contents heldObjectContents;
	Contents secondObjContents;
	Contents rightClickedContents;

	[Header ("Details Panel")]
	public GameObject contentDetailsPanel;
	public InputField containerInputField;
	public Text containerNameText, contentVolumeText, contentItemsText;

	[Header ("Transfer Option Panel")]
	public GameObject transferOptionsPanel;
	public Text transferContainerNameHeldText, transferVolumeHeldText;
	public Text transferContainerNameDestText, transferVolumeDestText;
	public Text minTransfer, maxTransfer;
	public InputField currentTransfer;
	public Slider sliderTransferValue;

	[Header ("Pipette Fill Panel")]
	public GameObject pipetteTransferPanel;
	public Text pipTransferContainerNameText, pipTransferVolumeText;
	public Text pipTransferContainerNameDestText, pipTransferVolumeDestText;
	public Text pipMinTransfer, pipMaxTransfer;
	public InputField pipCurrentTransfer;
	public Slider pipSliderValue;

    [Header ("Transfer All Panel")]
    public GameObject transferAllPanel;
    public Text transferAllText;
    public Text transferAllHeldText, transferAllOtherText;

	[Header ("Tick and Cross")]
	public GameObject tick, cross;

	[Header ("Prefabs for testing")]
	public GameObject[] testPrefabs;

    public delegate void ChangeColliderState ();
    public static event ChangeColliderState OnColliderStateChange;

	///<summary>Add event listeners here.</summary>
	void Start () {
		contentDetailsPanel.SetActive (false);
		transferOptionsPanel.SetActive (false);
		pipetteTransferPanel.SetActive (false);

		pipCurrentTransfer.onEndEdit.AddListener (delegate {PipInputValueChanged ();});
		currentTransfer.onEndEdit.AddListener (delegate {InputValueChanged ();});
		containerInputField.onEndEdit.AddListener (delegate {ContainerInputFieldChanged ();});

		HideGreenTick ();
		HideRedCross ();
	}

	///<summary>Remove event listeners here.</summary>
	void OnDisable () {
		pipCurrentTransfer.onEndEdit.RemoveAllListeners ();
		currentTransfer.onEndEdit.RemoveAllListeners ();
		containerInputField.onEndEdit.RemoveAllListeners ();
	}

	void Update () {
        if (!transferring) {
            if (Input.GetMouseButtonDown (0)) {
                //AttemptSelection ();
                if (!transferring) {
                    ClickStart ();
                }
            }

            if (Input.GetMouseButtonUp (0)) {
                if (!transferring) {
                    ClickReleased ();
                }
            }
        }

		if (Input.GetMouseButtonDown (1)) {
			DisplayContentsDetails ();
		}

		if (selectedObject != null && transferring == false) {
			selectedObject.transform.position = new Vector3 (Camera.main.ScreenToWorldPoint (Input.mousePosition).x, Camera.main.ScreenToWorldPoint (Input.mousePosition).y, 0);
		}

		tick.transform.position = new Vector3 (Camera.main.ScreenToWorldPoint (Input.mousePosition).x, Camera.main.ScreenToWorldPoint (Input.mousePosition).y, -3);
		cross.transform.position = new Vector3 (Camera.main.ScreenToWorldPoint (Input.mousePosition).x, Camera.main.ScreenToWorldPoint (Input.mousePosition).y, -3);

		//if the selection is stuck and user cannot un-stick it, this will return control to them.
		if (Input.GetKeyDown (KeyCode.Escape)) {
			DropHeldObject ();
		}
	}

    void ClickStart () {
        RaycastHit2D hit = Physics2D.Raycast (Camera.main.ScreenToWorldPoint (Input.mousePosition), Vector2.zero);

        if (hit.collider != null) {
            if (selectedObject == null) {
                if (hit.transform.tag == "Solution" || hit.transform.tag == "Pipette" || hit.transform.tag == "Beaker" || hit.transform.tag == "Cylinder" || hit.transform.tag == "Stirrer" || hit.transform.tag == "StirringUtencil" || hit.transform.tag == "PhMeter" || hit.transform.tag == "PhRecorder") {
                    if (hit.transform.parent != null) {
                        hit.transform.parent = null;
                    }
                    selectedObject = hit.transform.gameObject;
                    heldObjectPreviousTag = hit.transform.tag;
                    selectedObject.tag = "Held";
                    selectedObject.GetComponent<BoxCollider2D> ().enabled = false;
                }
            }
        }
    }

    void ClickReleased () {
        RaycastHit2D hit = Physics2D.Raycast (Camera.main.ScreenToWorldPoint (Input.mousePosition), Vector2.zero);
        if (hit.collider != null && selectedObject != null) {
            //anything clicking on recycle bin
            if (hit.transform.tag == "Bin") {
                //display prompt?
                DestroyHeldObject ();
                return;
            }

            //holding the ph recorder, clicking on the ph meter will holster it (by childing it)
            if (heldObjectPreviousTag == "PhRecorder" && hit.transform.tag == "PhMeter") {
                selectedObject.transform.parent = hit.transform;
                selectedObject.transform.localPosition = hit.transform.gameObject.GetComponent<PhMeter> ().HolsteredPosition.localPosition;
                DropHeldObject ();
            }

            //holding a pipette, clicking on an acid container
            if (heldObjectPreviousTag == "Pipette" && hit.transform.tag == "Solution") {
                heldObjectContents = selectedObject.GetComponent<Contents> ();
                secondObjContents = hit.transform.GetComponent<Contents> ();

                //we don't want to mix contents in the pipette, plus it isn't practical IRL
                if (heldObjectContents.CurrentVolume == 0f) {
                    transferring = true;
                    DisplayPipetteTransferDetails ();
                } else {
                    secondObjContents = null;
                }
                selectedObject.GetComponent<BoxCollider2D> ().enabled = false;
            }

            //holding a pipette, clicking on a beaker
            if (heldObjectPreviousTag == "Pipette" && hit.transform.tag == "Beaker") {
                heldObjectContents = selectedObject.GetComponent<Contents> ();
                secondObjContents = hit.transform.GetComponent<Contents> ();
                transferring = true;
                //DisplayTransferDetails ();
                DisplayFullTransferDetails ();
                selectedObject.GetComponent<BoxCollider2D> ().enabled = false;
            }

            //holding a measuring cylinder, clicking on a beaker
            if (heldObjectPreviousTag == "Cylinder" && hit.transform.tag == "Beaker") {
                heldObjectContents = selectedObject.GetComponent<Contents> ();
                secondObjContents = hit.transform.GetComponent<Contents> ();
                transferring = true;
                if (secondObjContents.CurrentVolume == 0) {
                    DisplayTransferDetails ();
                } else {
                    DisplayFullTransferDetails ();
                }
                selectedObject.GetComponent<BoxCollider2D> ().enabled = false;
            }

            //holding a container, clicking on a cylinder
            if (heldObjectPreviousTag == "Solution" && hit.transform.tag == "Cylinder") {
                heldObjectContents = selectedObject.GetComponent<Contents> ();
                secondObjContents = hit.transform.GetComponent<Contents> ();
                transferring = true;
                
                if (secondObjContents.CurrentVolume == 0 || (heldObjectContents.contents == secondObjContents.contents && heldObjectContents.secondarySolutionName == "" && secondObjContents.secondarySolutionName == "")) {
                    DisplayTransferDetails ();
                } else {
                    DisplayFullTransferDetails ();
                }
                selectedObject.GetComponent<BoxCollider2D> ().enabled = false;
            }

            //holding a beaker, clicking on measuring cylinder
            if (heldObjectPreviousTag == "Beaker" && hit.transform.tag == "Cylinder") {
                heldObjectContents = selectedObject.GetComponent<Contents> ();
                secondObjContents = hit.transform.GetComponent<Contents> ();
                transferring = true;
                if (secondObjContents.CurrentVolume == 0) {
                    DisplayTransferDetails ();
                } else {
                    DisplayFullTransferDetails ();
                }
                selectedObject.GetComponent<BoxCollider2D> ().enabled = false;
            }

            //holding a beaker, clicking on the stirrer
            if (heldObjectPreviousTag == "Beaker" && hit.transform.tag == "Stirrer") {
                //the stirrer object MUST have a child object at index 0 so we can snap our object to its position
                heldObjectContents = selectedObject.GetComponent<Contents> ();
                selectedObject.transform.parent = hit.transform;
                selectedObject.transform.localPosition = hit.transform.GetChild (0).localPosition;

                if (heldObjectContents.CurrentVolume > 0f) {
                    heldObjectContents.mixtureState = MixtureState.Mixed;
                }

                DropHeldObject ();
            }

            //holding a stirring utencil, clicking on a beaker
            if (heldObjectPreviousTag == "StirringUtencil" && hit.transform.tag == "Beaker") {
                selectedObject.transform.parent = hit.transform;
                selectedObject.transform.localPosition = hit.transform.GetChild (0).localPosition;

                if (hit.transform.parent.tag == "Stirrer") {
                    hit.transform.GetComponent<Contents> ().mixtureState = MixtureState.Mixed;
                }

                DropHeldObject ();
            }

            //holding a Ph Recorder, clicking on a beaker
            if (heldObjectPreviousTag == "PhRecorder" && hit.transform.tag == "Beaker") {
                selectedObject.transform.parent = hit.transform;
                selectedObject.transform.localPosition = hit.transform.GetChild (0).localPosition;

                Contents beakerContents = hit.transform.GetComponent<Contents> ();
                if (beakerContents.mixtureState == MixtureState.Mixed || beakerContents.mixtureState == MixtureState.Undiluted) {
                    GameObject phMeter = selectedObject.GetComponent<PhRecorder> ().parentMeter;
                    phMeter.GetComponent<PhMeter> ().SetText (beakerContents.PH);
                }

                DropHeldObject ();
            }
        } else {
            if (selectedObject != null) {
                Debug.Log ("Nothing there - dropping");
                DropHeldObject ();
            }
        }
    }

	///<summary>Attempts to do primary operation on left click, ie selecting objects or executing an action on another object when one is held.</summary>
	void AttemptSelection () {
        Debug.Log ("Attempting selection");
		RaycastHit2D hit = Physics2D.Raycast (Camera.main.ScreenToWorldPoint (Input.mousePosition), Vector2.zero);
		
		if (hit.collider != null) {
			//Debug.Log (hit.collider.gameObject);
			
			//if there is no currently held object, pick it up
			if (selectedObject == null) {
				if (hit.transform.tag == "Solution" || hit.transform.tag == "Pipette" || hit.transform.tag == "Beaker" || hit.transform.tag == "Cylinder" || hit.transform.tag == "Stirrer" || hit.transform.tag == "StirringUtencil" || hit.transform.tag == "PhMeter" || hit.transform.tag == "PhRecorder") {
					if (hit.transform.parent != null) {
						hit.transform.parent = null;
					}
					selectedObject = hit.transform.gameObject;
					heldObjectPreviousTag = hit.transform.tag;
					selectedObject.tag = "Held";
					selectedObject.GetComponent<BoxCollider2D> ().enabled = false;
				}
				
			} else {
				//anything clicking on recycle bin
				if (hit.transform.tag == "Bin") {
					//display prompt?
					DestroyHeldObject ();
					return;
				}

				//holding the ph recorder, clicking on the ph meter will holster it (by childing it)
				if (heldObjectPreviousTag == "PhRecorder" && hit.transform.tag == "PhMeter") {
					selectedObject.transform.parent = hit.transform;
					selectedObject.transform.localPosition = hit.transform.gameObject.GetComponent <PhMeter> ().HolsteredPosition.localPosition;
					DropHeldObject ();
				}

				//holding a pipette, clicking on an acid container
				if (heldObjectPreviousTag == "Pipette" && hit.transform.tag == "Solution") {
					heldObjectContents = selectedObject.GetComponent<Contents> ();
					secondObjContents = hit.transform.GetComponent<Contents> ();

					//we don't want to mix contents in the pipette, plus it isn't practical IRL
					if (heldObjectContents.CurrentVolume == 0f) {
						transferring = true;
						DisplayPipetteTransferDetails ();
					} else {
						secondObjContents = null;
					}
                    DropHeldObjectKeepContents ();
                }

				//holding a pipette, clicking on a beaker
				if (heldObjectPreviousTag == "Pipette" && hit.transform.tag == "Beaker") {
					heldObjectContents = selectedObject.GetComponent<Contents> ();
					secondObjContents = hit.transform.GetComponent<Contents> ();
					transferring = true;
					DisplayFullTransferDetails ();
                    DropHeldObjectKeepContents ();
                }

				//holding a measuring cylinder, clicking on a beaker
				if (heldObjectPreviousTag == "Cylinder" && hit.transform.tag == "Beaker") {
					heldObjectContents = selectedObject.GetComponent<Contents> ();
					secondObjContents = hit.transform.GetComponent<Contents> ();
					transferring = true;
                    if (secondObjContents.CurrentVolume == 0) {
                        DisplayTransferDetails ();
                    } else {
                        DisplayFullTransferDetails ();
                    }
                    DropHeldObjectKeepContents ();
                }

				//holding a container, clicking on a cylinder
				if (heldObjectPreviousTag == "Solution" && hit.transform.tag == "Cylinder") {
					heldObjectContents = selectedObject.GetComponent<Contents> ();
					secondObjContents = hit.transform.GetComponent<Contents> ();
					transferring = true;
                    if (secondObjContents.CurrentVolume == 0) {
                        DisplayTransferDetails ();
                    } else {
                        DisplayFullTransferDetails ();
                    }
                    DropHeldObjectKeepContents ();
                }

				//holding a beaker, clicking on measuring cylinder
				if (heldObjectPreviousTag == "Beaker" && hit.transform.tag == "Cylinder") {
					heldObjectContents = selectedObject.GetComponent<Contents> ();
					secondObjContents = hit.transform.GetComponent<Contents> ();
					transferring = true;
                    if (secondObjContents.CurrentVolume == 0) {
                        DisplayTransferDetails ();
                    } else {
                        DisplayFullTransferDetails ();
                    }
                    DropHeldObjectKeepContents ();
				}

				//holding a beaker, clicking on the stirrer
				if (heldObjectPreviousTag == "Beaker" && hit.transform.tag == "Stirrer") {
					//the stirrer object MUST have a child object at index 0 so we can snap our object to its position
					selectedObject.transform.parent = hit.transform;
					selectedObject.transform.localPosition = hit.transform.GetChild (0).localPosition;
                    heldObjectContents.mixtureState = MixtureState.Mixed;

					DropHeldObject ();
				}

				//holding a stirring utencil, clicking on a beaker
				if (heldObjectPreviousTag == "StirringUtencil" && hit.transform.tag == "Beaker") {
					selectedObject.transform.parent = hit.transform;
					selectedObject.transform.localPosition = hit.transform.GetChild (0).localPosition;

					if (hit.transform.parent.tag == "Stirrer") {
						hit.transform.GetComponent<Contents> ().mixtureState = MixtureState.Mixed;
					}
					
					DropHeldObject ();
				}

				//holding a Ph Recorder, clicking on a beaker
				if (heldObjectPreviousTag == "PhRecorder" && hit.transform.tag == "Beaker") {
					selectedObject.transform.parent = hit.transform;
					selectedObject.transform.localPosition = hit.transform.GetChild (0).localPosition;

					Contents beakerContents = hit.transform.GetComponent <Contents> ();
					if (beakerContents.mixtureState == MixtureState.Mixed || beakerContents.mixtureState == MixtureState.Undiluted) {
						GameObject phMeter = selectedObject.GetComponent <PhRecorder> ().parentMeter;
						phMeter.GetComponent <PhMeter> ().SetText (beakerContents.PH);
					}

					DropHeldObject ();
				}
			}
		} else {
			DropHeldObject ();
		}
	}

	///<summary>Drops the currently held object, if there is one, at the mouse x and y position.</summary>
	void DropHeldObject () {
      Debug.Log ("drop held object");
		if (selectedObject != null) {
			if (heldObjectPreviousTag == "PhRecorder") {
				selectedObject.GetComponent <PhRecorder> ().ResetParentText ();
			}
			selectedObject.tag = heldObjectPreviousTag;
			selectedObject.GetComponent<BoxCollider2D> ().enabled = true;
			ClampObjectDroppedPosition ();
			selectedObject = null;
		}
	}

	///<summary></summary>
   void DropHeldObjectKeepContents () {
      if (selectedObject != null) {
         if (heldObjectPreviousTag == "PhRecorder") {
            selectedObject.GetComponent<PhRecorder> ().ResetParentText ();
         }
         selectedObject.tag = heldObjectPreviousTag;
         //selectedObject.GetComponent<BoxCollider2D> ().enabled = true;
			//ClampObjectDroppedPosition ();
         selectedObject = null;
      }
   }

	///<summary></summary>
	void ClampObjectDroppedPosition () {
		Vector3 droppedPos = Camera.main.WorldToViewportPoint (selectedObject.transform.position);
		Debug.Log (droppedPos);
		if (droppedPos.x < 0.04f) {
			droppedPos.x = 0.06f;
		} else if (droppedPos.x > 0.96f) {
			droppedPos.x = 0.94f;
		}
		if (droppedPos.y < 0.05f) {
			droppedPos.y = 0.08f;
		} else if (droppedPos.y > 0.95f) {
			droppedPos.y = 0.9f;
		}
		
		selectedObject.transform.position = Camera.main.ViewportToWorldPoint (new Vector3 (droppedPos.x, droppedPos.y, droppedPos.z));
		Debug.Log (Camera.main.ViewportToWorldPoint (new Vector3 (droppedPos.x, droppedPos.y, droppedPos.z)));
	}

	///<summary>Destroys the currently held object, if there is one.</summary>
	void DestroyHeldObject () {
		if (selectedObject != null && !transferring) {
			Destroy (selectedObject);
			selectedObject = null;
			HideGreenTick ();
			HideRedCross ();
		}
	}

    public void HideContentsDetails () {
        contentDetailsPanel.SetActive (false);
        rightClickedContents = null;
    }

	///<summary>Attempts to display the details of the selected solution.</summary>
	void DisplayContentsDetails () {
		RaycastHit2D hit = Physics2D.Raycast (Camera.main.ScreenToWorldPoint (Input.mousePosition), Vector2.zero);

		if (contentDetailsPanel.activeSelf) {
			contentDetailsPanel.SetActive (false);
			rightClickedContents = null;
		} else {
			if (selectedObject != null) {
				Contents c = selectedObject.GetComponent<Contents> ();
				if (c != null) {
                    rightClickedContents = c;
					containerInputField.text = rightClickedContents.setName;
					containerNameText.text = rightClickedContents.ToString ();
					contentVolumeText.text = rightClickedContents.CurrentVolume.ToString () + "mL / " + rightClickedContents.maxVolume.ToString () + "mL";
					if (rightClickedContents.CurrentVolume > 0) {
						string s = "";
						/*foreach (string item in rightClickedContents.solutionNamesAndMolarity) {
							string[] pieces = item.Split (' ');
							for (int i = 1; i < pieces.Length; i++) {
								s += pieces[i] + " ";
							}
							s += "M\n";
						}*/
                        foreach (KeyValuePair<string, float> item in rightClickedContents.solutionVolumes) {
                            s += item.Value + "mL " + item.Key + "M\n";
                        }
						contentItemsText.text = s;
					} else {
						contentItemsText.text = "";
					}
					contentDetailsPanel.SetActive (true);
				}
			} else {
				if (hit.collider != null) {
					Contents c = hit.transform.gameObject.GetComponent<Contents> ();
					if (c != null) {
                        rightClickedContents = c;
						containerInputField.text = rightClickedContents.setName;
						containerNameText.text = rightClickedContents.ToString ();
						contentVolumeText.text = rightClickedContents.CurrentVolume.ToString () + "mL / " + rightClickedContents.maxVolume.ToString () + "mL";
						if (rightClickedContents.CurrentVolume > 0) {
							string s = "";
                            /*foreach (string item in rightClickedContents.solutionNamesAndMolarity) {
								string[] pieces = item.Split (' ');
								for (int i = 1; i < pieces.Length; i++) {
									s += pieces[i] + " ";
								}
								s += "M\n";
							}*/
                            foreach (KeyValuePair<string, float> item in rightClickedContents.solutionVolumes) {
                                if (item.Key != "Distilled Water")
                                    s += item.Value + "mL " + item.Key + "M\n";
                                else
                                    s += item.Value + "mL " + item.Key + "\n";
                            }
                            contentItemsText.text = s;
						} else {
							contentItemsText.text = "";
						}
						contentDetailsPanel.SetActive (true);
					}
				}
			}
		}
	}

    /// <summary>
    /// Used when we want to transfer ALL the contents of our held object to another object
    /// </summary>
    void DisplayFullTransferDetails () {
        OnColliderStateChange ();
        transferring = true;
        transferAllPanel.SetActive (true);
        transferAllText.text = "This will transfer " + heldObjectContents.CurrentVolume.ToString () + "mL to " + secondObjContents.tag + ". Click OK below to proceed.";
        transferAllHeldText.text = heldObjectContents.setName;
        transferAllOtherText.text = secondObjContents.setName;
    }

	///<summary>Used when we want to transfer the contents of our held object, ie the pipette, to another object, ie a cylinder.</summary>
	void DisplayTransferDetails () {
        OnColliderStateChange ();
        transferring = true;
        transferOptionsPanel.SetActive (true);
		transferContainerNameHeldText.text = heldObjectContents.setName;
		transferVolumeHeldText.text = heldObjectContents.CurrentVolume.ToString () + "mL";

		transferContainerNameDestText.text = secondObjContents.setName;
		transferVolumeDestText.text = secondObjContents.CurrentVolume.ToString () + "mL";

		float remainingVolume = secondObjContents.maxVolume - secondObjContents.CurrentVolume;
		if (remainingVolume > heldObjectContents.CurrentVolume) {
			maxTransfer.text = heldObjectContents.CurrentVolume.ToString ("0.00") + "mL";
			minTransfer.text = heldObjectContents.minVolume.ToString ("0.00") + "mL";
			sliderTransferValue.maxValue = heldObjectContents.CurrentVolume;
			sliderTransferValue.minValue = secondObjContents.minVolume;
			sliderTransferValue.value = sliderTransferValue.minValue;
			currentTransfer.text = secondObjContents.minVolume.ToString ();
		} else {
			maxTransfer.text = remainingVolume.ToString ("0.00") + "mL";
			minTransfer.text = secondObjContents.minVolume.ToString ("0.00") + "mL";
			sliderTransferValue.maxValue = remainingVolume;
			sliderTransferValue.minValue = secondObjContents.minVolume;
			sliderTransferValue.value = sliderTransferValue.minValue;
			currentTransfer.text = secondObjContents.minVolume.ToString ();
		}
	}

	///<summary>Almost the opposite of DisplayTransferDetails, where we are collecting a portion of a solution from a container into the held pipette.</summary>
	void DisplayPipetteTransferDetails () {
        OnColliderStateChange ();
        transferring = true;
		Debug.Log ("Displaying pipette transfer details panel");
		pipetteTransferPanel.SetActive (true);

		pipTransferContainerNameText.text = secondObjContents.setName;
		pipTransferVolumeText.text = secondObjContents.CurrentVolume.ToString () + "mL / " + secondObjContents.maxVolume.ToString () + "mL";

		pipTransferContainerNameDestText.text = heldObjectContents.setName;
		pipTransferVolumeDestText.text = heldObjectContents.CurrentVolume.ToString () + "mL / " + heldObjectContents.maxVolume.ToString () + "mL";

		float remainingVolume = heldObjectContents.maxVolume - heldObjectContents.CurrentVolume;
		if (remainingVolume > secondObjContents.CurrentVolume) {
			pipMinTransfer.text = heldObjectContents.minVolume.ToString ("0.00") + "mL";
			pipMaxTransfer.text = secondObjContents.CurrentVolume.ToString ("0.00") + "mL";
			pipSliderValue.maxValue = secondObjContents.CurrentVolume;
			pipSliderValue.minValue = heldObjectContents.minVolume;
			pipSliderValue.value = heldObjectContents.minVolume;
			pipCurrentTransfer.text = heldObjectContents.minVolume.ToString ();
		} else {
			pipMinTransfer.text = heldObjectContents.minVolume.ToString ("0.00") + "mL";
			pipMaxTransfer.text = remainingVolume.ToString ("0.00") + "mL";
			pipSliderValue.maxValue = remainingVolume;
			pipSliderValue.minValue = heldObjectContents.minVolume;
			pipSliderValue.value = heldObjectContents.minVolume;
			pipCurrentTransfer.text = heldObjectContents.minVolume.ToString ();
		}
	}

	///<summary>Triggered when the user is trying to fill a grounded object from a held pipette.</summary>
	public void MixHeldContentsToGroundedObject () {
        float amountToTransfer = 0f;
        float.TryParse (currentTransfer.text, out amountToTransfer);

        if (heldObjectContents.CurrentVolume >= amountToTransfer) {
            secondObjContents.AddMixtureToContainer (heldObjectContents, amountToTransfer);

            if (!heldObjectContents.usePKA || heldObjectContents.contents != "Distilled Water") {
                heldObjectContents.RemoveContentsByPercentage (amountToTransfer);
            }
        }
        transferOptionsPanel.SetActive (false);
        sliderTransferValue.value = 0f;
        currentTransfer.text = "0";
        PostMix ();
    }

    /// <summary>
    /// Resets selected object data, second object contents, starts a timer
    /// for the transferring bool reset, and calls OnColliderStateChange event
    /// Call this after you have exchanged volumes between reagents.
    /// </summary>
    void PostMix () {
        //the box collider here has to be set to false, as it will be flipped
        //later from the OnColliderStateChange event.
        selectedObject.GetComponent<BoxCollider2D> ().enabled = false;
        selectedObject.tag = heldObjectPreviousTag;
        selectedObject = null;
        heldObjectContents = null;
        secondObjContents = null;
        StartCoroutine (DelayTransferChange ());
        OnColliderStateChange ();
    }

    public void MixAllContentsToGroundedObject () {
        secondObjContents.AddMixtureToContainer (heldObjectContents, heldObjectContents.CurrentVolume);
        if (!heldObjectContents.usePKA || heldObjectContents.contents != "Distilled Water") {
            heldObjectContents.RemoveContentsByPercentage (heldObjectContents.CurrentVolume);
        }
        transferAllPanel.SetActive (false);
        PostMix ();
    }

	///<summary>Triggered when the user is trying to fill a pipette (held) from another solution (grounded).</summary>
	public void FillPipette () {
		float amountToTransfer = 0f;
		float.TryParse (pipCurrentTransfer.text, out amountToTransfer);

		if (secondObjContents.CurrentVolume >= amountToTransfer) {
			heldObjectContents.AddMixtureToContainer (secondObjContents, amountToTransfer);
            //secondObjContents.ChangeVolumeOfContents (-amountToTransfer);
            secondObjContents.RemoveContentsByPercentage (amountToTransfer);
		}
        pipetteTransferPanel.SetActive (false);
		pipSliderValue.value = 0f;
		pipCurrentTransfer.text = "0";
        PostMix ();
    }

	///<summary>Changes the transferring bool to false, so we can drop the currently held items.</summary>
	public void CancelButtonPressed () {
        //transferring = false;
        OnColliderStateChange ();
        StartCoroutine (DelayTransferChange ());

        DropHeldObject ();
        secondObjContents = null;
    }

    /// <summary>
    /// Delays the value flip of the transferring bool by 0.1 seconds.
    /// </summary>
    /// <returns></returns>
    IEnumerator DelayTransferChange () {
        yield return new WaitForSeconds (0.1f);
        transferring = false;
    }

	///<summary>Triggered when the input field for other transfers is changed.</summary>
	void InputValueChanged () {
		float sliderValue = 0f;
		float.TryParse (currentTransfer.text, out sliderValue);
		sliderTransferValue.value = sliderValue;
		ClampTransferValues ();
	}

	///<summary>Clamps UI values when transferring.</summary>
	void ClampTransferValues () {
		float min, max;
		min = (float) Convert.ToDouble (minTransfer.text.TrimEnd ('m','L'));
		max = (float) Convert.ToDouble (maxTransfer.text.TrimEnd ('m','L'));
		sliderTransferValue.value = Mathf.Clamp (sliderTransferValue.value, min, max);
		currentTransfer.text = sliderTransferValue.value.ToString ("0.00");
	}

	///<summary>Triggered when the input field for pip transfer is changed.</summary>
	void PipInputValueChanged () {
		/* float sliderValue = 0f;
		float.TryParse (pipCurrentTransfer.text, out sliderValue);
		pipSliderValue.value = sliderValue;
		ClampPipetteTransferInValues ();*/

		float sliderValue = 0f;
		//Debug.Log (sliderValue);
		float.TryParse (pipCurrentTransfer.text, out sliderValue);
		//Debug.Log (pipCurrentTransfer.text);
		//Debug.Log (sliderValue);
		pipSliderValue.value = sliderValue;
		//Debug.Log (pipSliderValue.value);

		float min, max;
		min = (float) Convert.ToDouble (pipMinTransfer.text.TrimEnd ('m','L'));
		//Debug.Log (min);
		max = (float) Convert.ToDouble (pipMaxTransfer.text.TrimEnd ('m','L'));
		//Debug.Log (max);
		//Debug.Log (pipSliderValue.value);
		pipSliderValue.value = Mathf.Clamp (pipSliderValue.value, min, max);
		//Debug.Log (pipSliderValue.value);
		pipCurrentTransfer.text = pipSliderValue.value.ToString ("0.00");
		//Debug.Log (pipCurrentTransfer.text);
	}

	///<summary>Clamps UI values when filling a pipette only.</summary>
	/* void ClampPipetteTransferInValues () {
		float min, max;
		min = (float) Convert.ToDouble (pipMaxTransfer.text.TrimEnd ('m','L'));
		max = (float) Convert.ToDouble (pipMinTransfer.text.TrimEnd ('m','L'));
		pipSliderValue.value = Mathf.Clamp (pipSliderValue.value, min, max);
		pipCurrentTransfer.text = pipSliderValue.value.ToString ("0.000");
	}*/

	///<summary></summary>
	public void GrabObjectFromStorage (GameObject go) {
		DropHeldObject ();
		if (go.transform.parent != null) {
			go.transform.parent = null;
		}
		selectedObject = go;
		heldObjectPreviousTag = go.tag;
		selectedObject.tag = "Held";
		//selectedObject.GetComponent<BoxCollider2D> ().enabled = false;
	}

	///<summary>Input the tag of the object we are hovering over, that isn't being held (the missing box collider on the held object makes this possible).</summary>
	public void HoveringOverObject (string tag) {
		if (selectedObject == null) {
			if (tag != "Bin") {
				ShowGreenTick ();
			} else {
				ShowRedCross ();
			}
		} else {
            if (!transferring) {
                //Use the held objects tag to determine whether the other object is something we can interact with.
                switch (heldObjectPreviousTag) {
                    case "Pipette":
                        switch (tag) {
                            case "Bin":
                            case "Solution":
                            case "Beaker":
                                ShowGreenTick ();
                                break;
                            default:
                                ShowRedCross ();
                                break;
                        }
                        break;
                    case "Solution":
                        switch (tag) {
                            case "Bin":
                            case "Cylinder":
                                ShowGreenTick ();
                                break;
                            default:
                                ShowRedCross ();
                                break;
                        }
                        break;
                    case "Beaker":
                        switch (tag) {
                            case "Bin":
                            case "Stirrer":
                            case "Cylinder":
                                ShowGreenTick ();
                                break;
                            default:
                                ShowRedCross ();
                                break;
                        }
                        break;
                    case "StirringUtencil":
                        switch (tag) {
                            case "Beaker":
                            case "Bin":
                                ShowGreenTick ();
                                break;
                            default:
                                ShowRedCross ();
                                break;
                        }
                        break;
                    case "PhRecorder":
                        switch (tag) {
                            case "PhMeter":
                            case "Bin":
                            case "Beaker":
                                ShowGreenTick ();
                                break;
                            default:
                                ShowRedCross ();
                                break;
                        }
                        break;
                    case "PhMeter":
                        switch (tag) {
                            case "Bin":
                                ShowGreenTick ();
                                break;
                            default:
                                ShowRedCross ();
                                break;
                        }
                        break;
                    case "Cylinder":
                        switch (tag) {
                            case "Beaker":
                            case "Bin":
                                ShowGreenTick ();
                                break;
                            default:
                                ShowRedCross ();
                                break;
                        }
                        break;
                    case "Stirrer":
                        switch (tag) {
                            case "Bin":
                                ShowGreenTick ();
                                break;
                            default:
                                ShowRedCross ();
                                break;
                        }
                        break;
                }
            }
		}
	}

	void ContainerInputFieldChanged () {
		if (rightClickedContents == null && heldObjectContents != null) {
			heldObjectContents.setName = containerInputField.text;
		} else if (rightClickedContents != null && heldObjectContents == null) {
			rightClickedContents.setName = containerInputField.text;
		}
	}

	public void NotHovering () {
		HideGreenTick ();
		HideRedCross ();
	}

	void ShowGreenTick () {
		tick.SetActive (true);
		HideRedCross ();
	}

	void HideGreenTick () {
		tick.SetActive (false);
	}

	void ShowRedCross () {
		cross.SetActive (true);
		HideGreenTick ();
	}

	void HideRedCross () {
		cross.SetActive (false);
	}

}
