﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjSpawner : MonoBehaviour {

	[SerializeField] Color okColour, notOkColour;
	public int count, max;
	public GameObject objToSpawn;
	public Text counter;
	MouseSelection ms;

	void Start () {
		count = 0;
		ms = GameObject.FindGameObjectWithTag ("MainCamera").GetComponent <MouseSelection> ();
		UpdateText ();
	}
	
	///<summary>If the current count of the object this will spawn is less than the maximum, it creates a new gameobject of this item, adds to the counter and updates the counter text.</summary>
	public void ButtonClick () {
		//if (count < max) {
			GameObject item = Instantiate (objToSpawn) as GameObject;
			count++;
			UpdateText ();
			ms.GrabObjectFromStorage (item);
        item.transform.position = Vector3.zero;

			//Updates the newly spawned gameobject so it knows that we are waiting for it to be destroyed later.
			item.GetComponent <SpawnedObject> ().OverwriteParent (this);
		//}
	}

	///<summary>Called by 'child' objects when they are about to be destroyed, to change the counter.</summary>
	public void CopyDeleted () {
		count--;
		UpdateText ();
	}

	///<summary>Updates the counter UI element.</summary>
	void UpdateText () {
		counter.text = count + "/" + max;
		if (count < max) {
			counter.color = okColour;
		} else {
			counter.color = notOkColour;
		}
	}

}
