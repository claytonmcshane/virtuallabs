﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioToggle : MonoBehaviour {

	public AudioSource src;
	public Image currentImage;
	public Sprite unmutedSprite, mutedSprite;

	public void ToggleAudioPlayback () {
		if (src.volume == 0.5f) {
			src.volume = 0f;
			currentImage.sprite = mutedSprite;
		} else {
			src.volume = 0.5f;
			currentImage.sprite = unmutedSprite;
			src.Play ();
		}
	}

}
