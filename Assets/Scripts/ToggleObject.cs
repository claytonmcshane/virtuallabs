﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleObject : MonoBehaviour {

	public GameObject[] objectsToToggleVisiblity;

	public void ToggleVisibility () {
		foreach (GameObject go in objectsToToggleVisiblity) {
			go.SetActive (!go.activeSelf);
		}
	}

}
