﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/*
Figured I'd explain here to anyone who reads this at a later point.
It's a good habit to do getcomponents through code vs manually in the inspector.
The problem is, if the gameobject is disabled and you have more than 1 object
trying to access it and it's child components, they won't work.
What I've done here is I've left the parent object active but disabled the
image (background) and text components, and I just activate them below 
as necessary. This way, all objects running this script will be able to
locate and cache the required links without causing clashes with eachother.
*/

public class HoverDescription : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

    [SerializeField] GameObject objDescription;
    [SerializeField] Image descriptionPanel;
    [SerializeField] Text descriptionText;
    public string description = "";

    void Start () {
        objDescription = GameObject.FindGameObjectWithTag ("HoverDescription");
        descriptionPanel = objDescription.GetComponent<Image> ();
        descriptionText = objDescription.transform.GetChild (0).GetComponent<Text> ();
    }

    public void OnPointerEnter (PointerEventData eventData) {
        descriptionPanel.enabled = true;
        descriptionText.enabled = true;
        descriptionText.text = description;
    }

    public void OnPointerExit (PointerEventData eventData) {
        descriptionPanel.enabled = false;
        descriptionText.enabled = false;
    }
}
