﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Graphics : MonoBehaviour {

	void Start () {
		switch (QualitySettings.GetQualityLevel ()) {
			case 0:
			case 1:
				Application.targetFrameRate = 30;
				break;
			case 2:
			case 3:
				Application.targetFrameRate = 60;
				break;
			default:
				Application.targetFrameRate = -1;
				break;
		}
	}

}
