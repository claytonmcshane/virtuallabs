﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecycleBinPosition : MonoBehaviour {

	void Start () {
		Camera main = Camera.main;
        //Debug.Log (main.WorldToViewportPoint (transform.position).x);
        //Debug.Log (main.WorldToViewportPoint (transform.position).y);
        //this position was found by using the WorldToViewportPoint function on the 720p standalone setting
        //after that, we're just manually setting the position of the bin to the top left corner given these coordinates
        //if you want it in the top right corner, set the X value in the Vector3 below to (1 - 0.0449f)
        Vector3 binPos = main.ViewportToWorldPoint (new Vector3 (0.05843753f, 0.8941959f, 0f));
		//also need to reset the Z position here, otherwise it will be at the same Z position as the camera, and we won't be able to see it.
		binPos = new Vector3 (binPos.x, binPos.y, -8f);
		transform.position = binPos;

	}

}
